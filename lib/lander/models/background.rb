class Background
  attr_reader :y
  HEIGHT = 1500

  def initialize(window)
    @window = window
    @y = 100
    @rocket = @window.rocket
    @image = Gosu::Image.new(@window, "assets/bg.png", true, 0, 0, @window.width, HEIGHT)
  end

  def tick
    @y = @rocket.final_approach? ? HEIGHT - @window.height : @rocket.y + @rocket.image.height - 100
  end

  def draw
    @image.draw(0, -@y, 0)
  end
end
