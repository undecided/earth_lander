class Target
  attr_reader :x

  def initialize(window)
    @window = window
    @rocket = @window.rocket
    @image = Gosu::Image.new(@window, "assets/target.png", true, 0, 0, 100, 19)
  end

  def tick

  end

  def draw
    if @rocket.final_approach?
      @image.draw((@window.width / 2) - 50, @window.height - 20, 1)
    end
  end
end
