class Rocket
  attr_reader :x, :y, :visual_y, :x_velocity, :y_velocity, :image, :landed

  def initialize(window)
    @window = window
    @image = Gosu::Image.new(@window, "assets/rocket.png")
    @explosion = Gosu::Image.new(@window, "assets/explosion.png")
    @main_thruster = Gosu::Image.new(@window, "assets/main_thruster.png")
    @landed = false
    @x = (0..@window.width).to_a.sample
    @y = 0 # Bottom of rocket
    @y_velocity = 0
    @x_velocity = 0
  end

  def tick
    return if @landed
    @y_velocity += 9.8 * @window.tick
    @y += @y_velocity
    @x += @x_velocity
    @visual_y =  final_approach? ? @window.height - (distance_to_ground + @image.height): 100
    if distance_to_ground <= 0
      @landed = true
      if @y_velocity > 2 || @x_velocity > 2
        @image = @explosion
      end
      @y_velocity = 0
      @x_velocity = 0
      @visual_y = @window.height - (@image.height + 20)
    end
    @window.console.text = <<-END
x:
#{@x.round}
y:
#{@y.round}
x_velocity:
#{@x_velocity.round}
y_velocity:
#{@y_velocity.round}
distance_to_ground:
#{distance_to_ground.round}
    END
  end

  def fire_main
    return if @landed
    @y_velocity -= 15 * @window.tick
    @main_fired = true
  end


  def fire_left
    return if @landed
    @x_velocity += 7 * @window.tick
    @left_fired = true
  end

  def fire_right
    return if @landed
    @x_velocity -= 7 * @window.tick
    @right_fired = true
  end

  def draw
    @image.draw(@x - (@image.width / 2), @visual_y, 5)
    draw_main_thruster if @main_fired
    @main_fired = false
  end

  def draw_main_thruster
    @main_thruster.draw(@x - (@main_thruster.width / 2), @visual_y+80, 4)
  end

  def distance_to_ground
    Background::HEIGHT - (20 + @y)
  end

  def final_approach?
    distance_to_ground < @window.height + @image.height - 100
  end

  def landed?
    !!@landed
  end
end
