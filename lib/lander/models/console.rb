class Console
  def initialize(window, text)
    @window = window
    self.text = text
  end

  def text=(text)
    @image = Gosu::Image.from_text(@window, text, Gosu::default_font_name, 30, 2, 250, :center)
  end

  def tick
  end

  def draw
    @image.draw(@window.width - 250, 100, 2)
  end
end
