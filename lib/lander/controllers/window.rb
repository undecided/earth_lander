class Window < Gosu::Window
  attr_reader :rocket, :target, :background, :console, :tick

  def initialize
    super 800, 600, false
    self.caption = "Earth Lander"
    restart
  end

  def update
    @tick = time_since_last_tick

    @player.tick

    @rocket.tick
    @background.tick
    @target.tick
    @console.tick
  end

  def draw
    @background.draw
    @console.draw
    @target.draw
    @rocket.draw
  end


  def button_down(id)
    if id == Gosu::KbEscape
      close
    elsif id == Gosu::KbReturn
      restart
    end
  end

  def restart
    @console = Console.new(self, "Initializing...")
    @rocket = Rocket.new(self)
    @target = Target.new(self)
    @background = Background.new(self)

    @player = ProgrammedPlayer.new(self)
    @player = KeyboardPlayer.new(self) unless @player.respond_to? :tick
  end

  def time_since_last_tick
    new_tick = Time.now.to_f
    @last_tick ||= new_tick
    out = new_tick - @last_tick
    @last_tick = new_tick
    out
  end
end
