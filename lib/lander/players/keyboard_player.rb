class KeyboardPlayer
  def initialize(window)
    @window = window
    @rocket = @window.rocket
  end

  def tick
    @rocket.fire_main if button_down? Gosu::KbUp
    @rocket.fire_right if button_down? Gosu::KbLeft
    @rocket.fire_left if button_down? Gosu::KbRight
  end

  def button_down?(*buttons)
    buttons.find { |btn| @window.button_down? btn }
  end

end
