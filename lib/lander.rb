require 'rubygems'
require 'bundler/setup'
Bundler.require(:default)
require_relative "lander/version"
require_relative "lander/controllers/window"
require_relative "lander/models/background"
require_relative "lander/models/console"
require_relative "lander/models/target"
require_relative "lander/models/rocket"
require_relative "lander/players/keyboard_player"
require_relative "lander/players/programmed_player"


module Lander
  def self.start
    Window.new.show
  end
end
