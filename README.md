# Earth Lander

You pilot a rocket, finding its way to a target on the earth.

Yeah, it's THAT good.


## How it works

This was built as a simple test-driven game for NRUG March 2015. You can play the game with the arrow keys on your keyboard, and the tests fail to start with. Once you've implemented a tick method, keyboard input no longer works.

The aim is to land on the target slow enough that you don't crash and burn in a loud inferno of death, destruction and misery. Oh, and the rocket. Don't want that crashing either.

If you've crashed, press "Return" to start again. To exit, close the window, hit ctrl-c in the terminal, dispose of your computer in an acid pit or press escape within the game.


## Installation

Clone the repo, then run 

    bundle install

Note: this involves installing gosu, which has a few non-ruby dependencies. Visit https://github.com/jlnr/gosu/wiki for the lowdown.

Start the game with:

    rake start

or run the tests with

    rspec



## Contributing

Feel free to tell us how your group got on, or to improve it in some way

1. Fork it
2. Create your group's branch (`git checkout -b XRUG-jun-2032`)
3. Commit your changes (`git commit -am 'We did this and it waz ORRESUM'`)
4. Push to the branch (`git push origin XRUG-jun-2032`)
5. Create new Pull Request
