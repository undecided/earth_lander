require_relative 'spec_helper'

# You will receive a world in your constructor (world).
# You will need to mock the following methods / objects:
#
#  - world.rocket.distance_to_ground
#  - world.rocket.y_velocity
#  - world.rocket.landed?  # false if flying, true to indicate that it has landed or crashed
#
#  You will then want to expect your rocket to receive, or not to receive, fire_main:
#
#  - world.rocket.fire_main
#
# Later, you might need to mock:
#
# - world.rocket.x
# - world.rocket.x_velocity
# - world.target.x
#
# and receive
#
# - world.rocket.fire_left  # moves the rocket right
# - world.rocket.fire_right # moves the rocket left
#
# If you have time: Imagine your rocket has finite fuel supplies. How can you make your algorithm better?

describe ProgrammedPlayer do
  subject { ProgrammedPlayer.new double('world') }

  context 'as a valid player' do
    it "can be instantiated" do
      expect(subject).to be_a ProgrammedPlayer
    end

    it "accepts a tick" do
      expect { subject.tick }.not_to raise_error
    end
  end
end
